(function(){
    var CrudService = function ($http) {

        this.getItems = function (source) {
            var request = $http({
                method: "GET",
                url: "http://team-to-do.herokuapp.com/api/v1/"+ source
            });
            return request;
        };
        this.postItems = function (source,data) {
            var request = $http({
                method: "POST",
                url: "http://team-to-do.herokuapp.com/api/v1/"+ source,
                data:data
            });
            return request;
        };
        this.deleteItem = function (source) {
            var request = $http({
                method : "DELETE",
                url : "http://team-to-do.herokuapp.com/api/v1/"+ source
            });
            return request;
        };
        this.updateItem = function (source, data) {
            var request = $http({
                method: "PUT",
                url: "http://team-to-do.herokuapp.com/api/v1/"+ source,
                data: data
            });
            return request;
        }
    };
    App.service("CrudService",CrudService)
}());