(function () {

    var Auth = function ($http) {

        var source = "http://team-to-do.herokuapp.com/api/v1/access";

        this.login = function (record) {
            var request = $http({
                method: "POST",
                url: source,
                data: record
            });
            return request;
        };

        this.logout = function (record) {
            var request = $http({
                method: "DELETE",
                url: "http://team-to-do.herokuapp.com/api/v1/access/" + record
            });
            return request;
        }
    };

    App.service("AuthService",Auth)
}());