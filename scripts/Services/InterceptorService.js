(function () {
    var InterceptorService = function ($q) {
        return{
            request: function (config) {
                config.headers={
                    'Authorization':'Token token=' + localStorage.getItem('token'),
                    'Content-Type':'application/json'
                };
                return config;
            },
            response: function (result) {

                return result;
            },
            responseError: function (rejection) {
                return $q.reject(rejection);
            }
        }
    };

    App.service("InterceptorService",InterceptorService)
}());