(function () {
   var modalListCtrl = function ($scope, $modalInstance, CrudService, $location, list) {
       if(list){
           $scope.list = list;
       }
       $scope.createList = function () {
           $scope.list.user_id = window.localStorage.getItem('idLoggedUser');
           var data = {'list':$scope.list};
           var promise = CrudService.postItems('lists',data);
           promise.then(function (response) {
                   $modalInstance.close(response);
               },
               function (response) {
                   alert("nije prosla lista");
               });
       };



       $scope.updateList = function () {
            var data = {'list':$scope.list};
           var promise = CrudService.updateItem("lists/"+list.id, data);
           promise.then(function (response) {
                   $modalInstance.close(response);
           },
               function (response) {
                   alert("error while updating list")
               });
       };
   };
    App.controller("modalListCtrl",modalListCtrl)
}());