(function () {
    var modalTaskCtrl = function ($scope, $modalInstance, CrudService,$location, lists, task, $stateParams) {
        $scope.listForDropDown = lists;
        $scope.task =task;
        $scope.listDrop = Number($stateParams.id);
        if(task){$scope.listDrop = task.list_id;}

        $scope.createTask = function(){
            $scope.task.list_id = $stateParams.id;
            $scope.task.state = false;
            var data = {'task':$scope.task};
            var promise = CrudService.postItems('tasks',data);
            promise.then(function(response){
                $modalInstance.close(response);
            }, function (response) {
                alert('Post nece');
            });
        };

       $scope.updateTask = function () {

           $scope.task.list_id = $scope.listDrop.id;
           var data = {'task' : $scope.task};
           var promise = CrudService.updateItem('tasks/'+$scope.task.id,data);
           promise.then(function (response) {
                   $modalInstance.close(response);
           },
               function (response) {
                   alert("update ne radi");
               });

       };
    };

    App.controller("modalTaskCtrl",modalTaskCtrl)
}());