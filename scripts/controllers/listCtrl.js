(function () {
  var listCtrl = function ($scope, CrudService, $modal, $stateParams) {
      loadList();
      $scope.id = $stateParams.id;
     function loadList() {
        var promise = CrudService.getItems("lists");
        promise.then(function (response) {
                $scope.listsOfSpecificUser = response.data;
        },
            function (response) {
                alert("nece");
            });
    }
      $scope.setSelectedList = function(item){
          $scope.selectedList = item;
      };

      $scope.createListModal = function () {

          var modalInstance = $modal.open({
              templateUrl: 'views/createList.html',
              controller: 'modalListCtrl',
              resolve:{
                  list:function(){

                  }
              }
          });

          modalInstance.result.then(function (response) {
              $scope.listsOfSpecificUser.push(response.data);
          });
      };
      $scope.updateListModal = function () {
          var modalInstance = $modal.open({
              templateUrl: 'views/updateList.html',
              controller: 'modalListCtrl',
              resolve:{
                  list: function() {
                       var selectedListCopy = angular.copy($scope.selectedList);
                      return selectedListCopy;
                  }
              }
          });
          modalInstance.result.then(function () {
                  loadList();
              });

      };

      $scope.deleteList = function (list) {
          var promise = CrudService.deleteItem("lists/"+list.id);
          promise.then(
              function (response) {
                  loadList();
              },
              function (response) {
                  alert("delete failed");
              }
          );
      };
  };

    App.controller("listCtrl",listCtrl)
}());
