(function () {
   var taskCtrl = function ($scope, CrudService,$stateParams, $modal) {
       loadTasks();
     function loadTasks() {
          var promise = CrudService.getItems("lists/"+$stateParams.id);
           promise.then(function (response) {
                   $scope.tasksOfSpecificList = response.data.tasks;

           },
               function (response) {
                   alert("error");
               });
       }

       $scope.isChecked = function (task) {
           $scope.task = task;
           var data = {'task':{'state': $scope.task.state}};
           var promise = CrudService.updateItem('tasks/'+$scope.task.id, data);
           promise.then(function (response) {
               loadTasks();
           },
               function (response) {
                   alert("eror while updating task");
               });
       };

       $scope.getTasks = loadTasks();


       $scope.setClicked = function (task) {
         $scope.selectedTask = task;
       };

       $scope.deleteTask = function () {
           var promise = CrudService.deleteItem("tasks/"+$scope.selectedTask.id);
           promise.then(function (response) {
                   loadTasks();
               alert("delete uspjesan");
           },
               function (response) {
               alert("delete fail");
           });
       };

       $scope.createTaskModal = function () {
                var promise = CrudService.getItems("lists");
                promise.then(function (response) {
                       var  modalInstance = $modal.open({
                            templateUrl: 'views/createTask.html',
                            controller: 'modalTaskCtrl',
                            resolve:{
                                lists:function(){
                                   return response.data;
                                },
                                task: function(){

                                }
                            }
                        });
                        modalInstance.result.then(function (response) {
                            loadTasks();
                        });
                },
                    function (response) {
                    });


       };

       $scope.updateTaskModal = function (task) {
           var promise = CrudService.getItems("lists");
           promise.then(function(response){
               var  modalInstance = $modal.open({
                   templateUrl: 'views/updateTask.html',
                   controller: 'modalTaskCtrl',
                   resolve:{
                       lists: function(){
                           return response.data;
                       },
                       task: function(){
                           var taskCopy = angular.copy(task);
                           return taskCopy;
                       }
                   }
               });
               modalInstance.result.then(function (response) {
                $scope.tasksOfSpecificList.push(response.data);
               });
           });
       };
};
    App.controller("taskCtrl",taskCtrl)

}());