(function(){

    var authCtrl = function ($scope,AuthService,$state, CrudService) {
        $scope.login = function () {
            var user = $scope.user;
            var userJson = {"access": user};
            var promise = AuthService.login(userJson);
            promise.then(function (response) {
                window.localStorage.setItem('token', response.data.auth_token);
                window.localStorage.setItem('idLoggedUser', response.data.id);
                $state.go('lists');
            }, function (response) {
                alert("ne radi");
            });
        };

        $scope.registerUser = function () {
            var data = {'user': $scope.user};
            var promise = CrudService.postItems("users", data);
            promise.then(function (response) {
                $state.go('login');
            },
                function (response) {
                    alert("Error while registering new user");
                });
        };



    };


    App.controller("authCtrl",authCtrl)
}());