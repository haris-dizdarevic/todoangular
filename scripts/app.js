
var App =angular.module("testApp",['ngRoute','ui.bootstrap','ui.router']);

App.config(function ($stateProvider, $httpProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('test',{
            url: '/test',
            templateUrl: 'views/test.html',
            controller: 'testCtrl'
        })

        .state('login',{
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'authCtrl'
        })

        .state('lists',{
            url: '/lists',
            views:{
                'columnOne@':{
                    templateUrl: 'views/list.html',
                    controller: 'listCtrl'
                }
            }

        })

        .state('logout',{
            url:'/logout',
            templateUrl: '',
            controller: function (AuthService, $location) {
                id = window.localStorage.getItem('idLoggedUser');
                    var promise = AuthService.logout(id);
                    promise.then(function (response) {
                            localStorage.removeItem('token');
                            localStorage.removeItem('idLoggedUser');
                            $location.path("/login");
                            return response;
                        },
                        function (response) {
                            alert("error while logging out");
                        });
            }
        })
        .state('register',{
            url: '/register',
            templateUrl: 'views/userForm.html',
            controller: 'authCtrl'
        })
        .state('selectedList',{
            url: '/lists/:id',
            views:{
                'columnOne@':{
                    templateUrl: 'views/list.html',
                    controller: 'listCtrl'
                },
                'columnTwo@':{
                    templateUrl: 'views/task.html',
                    controller: 'taskCtrl'
                }
            }

        });
    $httpProvider.interceptors.push('InterceptorService');


});

App.run(["$rootScope", function($rootScope) {
    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {

            if (window.localStorage.getItem('token') != undefined) {
                $rootScope.logged = true;
                if (toState.name == 'login' || toState.name == 'register') {
                    event.preventDefault();
                }
            } else if (window.localStorage.getItem('token') == undefined) {
                $rootScope.logged = false;
                if (toState.name == 'lists' || toState.name == 'logout' || toState.name == 'selectedList') {
                    event.preventDefault();
                }
            }
        });
}]);

